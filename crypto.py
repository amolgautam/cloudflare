#importing libraries
import base64
import requests
import json
from cryptography.hazmat.primitives.asymmetric.x25519 import X25519PrivateKey, X25519PublicKey
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.ciphers.modes import CBC
from cryptography.hazmat.primitives.ciphers import (Cipher, algorithms, modes)
from cryptography.hazmat.primitives.serialization import NoEncryption


# Function to decrypt the encrypted bytes
def aes_cbc_decrypt(key, iv, enc_data):
    mode = modes.CBC(iv)
    cipher = Cipher(algorithms.AES(key), mode=mode)
    decryptor = cipher.decryptor()
    output = decryptor.update(enc_data) + decryptor.finalize()
    return output 

# generate pair of X25519 keys
private_key = X25519PrivateKey.generate()
public_key = private_key.public_key()
public_key_bytes = public_key.public_bytes(
    encoding=serialization.Encoding.Raw,
    format=serialization.PublicFormat.Raw
)
public_key_bytes_base64 = base64.b64encode(public_key_bytes,None)

url = 'https://challenge-5c28.szechuen.workers.dev'
params = public_key_bytes_base64

#sending request to the API endpoint
response = requests.post(url,params)
response_json = json.loads(response._content)


# Decode from base64
encrypted_msg_bytes = base64.b64decode(response_json['encryptedMsg'])
iv_bytes = base64.b64decode(response_json['iv'])
public_key_bytes = base64.b64decode(response_json['publicKey'])


#Trying to get the shared key between the peers
peer_X25519PublicKey = X25519PublicKey.from_public_bytes(public_key_bytes)
shared_key = private_key.exchange(peer_X25519PublicKey)

decrypted_data = aes_cbc_decrypt(shared_key, iv_bytes, encrypted_msg_bytes)
print(decrypted_data)